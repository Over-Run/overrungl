/*
 * MIT License
 *
 * Copyright (c) 2022-present Overrun Organization
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

// This file is auto-generated. DO NOT EDIT!
package overrungl.opengl.ext;

import overrungl.*;
import overrungl.opengl.*;
import java.lang.foreign.*;
import static java.lang.foreign.FunctionDescriptor.*;
import static java.lang.foreign.ValueLayout.*;
import static overrungl.opengl.GLLoader.*;

/**
 * {@code GL_OML_resample}
 */
public final class GLOMLResample {
    public static final int GL_PACK_RESAMPLE_OML = 0x8984;
    public static final int GL_UNPACK_RESAMPLE_OML = 0x8985;
    public static final int GL_RESAMPLE_REPLICATE_OML = 0x8986;
    public static final int GL_RESAMPLE_ZERO_FILL_OML = 0x8987;
    public static final int GL_RESAMPLE_AVERAGE_OML = 0x8988;
    public static final int GL_RESAMPLE_DECIMATE_OML = 0x8989;
}
