/*
 * MIT License
 *
 * Copyright (c) 2022-present Overrun Organization
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

// This file is auto-generated. DO NOT EDIT!
package overrungl.opengl.ext.ext;

import overrungl.*;
import overrungl.opengl.*;
import java.lang.foreign.*;
import static java.lang.foreign.FunctionDescriptor.*;
import static java.lang.foreign.ValueLayout.*;
import static overrungl.opengl.GLLoader.*;

/**
 * {@code GL_EXT_texture_snorm}
 */
public final class GLEXTTextureSnorm {
    public static final int GL_ALPHA_SNORM = 0x9010;
    public static final int GL_LUMINANCE_SNORM = 0x9011;
    public static final int GL_LUMINANCE_ALPHA_SNORM = 0x9012;
    public static final int GL_INTENSITY_SNORM = 0x9013;
    public static final int GL_ALPHA8_SNORM = 0x9014;
    public static final int GL_LUMINANCE8_SNORM = 0x9015;
    public static final int GL_LUMINANCE8_ALPHA8_SNORM = 0x9016;
    public static final int GL_INTENSITY8_SNORM = 0x9017;
    public static final int GL_ALPHA16_SNORM = 0x9018;
    public static final int GL_LUMINANCE16_SNORM = 0x9019;
    public static final int GL_LUMINANCE16_ALPHA16_SNORM = 0x901A;
    public static final int GL_INTENSITY16_SNORM = 0x901B;
    public static final int GL_RED_SNORM = 0x8F90;
    public static final int GL_RG_SNORM = 0x8F91;
    public static final int GL_RGB_SNORM = 0x8F92;
    public static final int GL_RGBA_SNORM = 0x8F93;
}
