/*
 * MIT License
 *
 * Copyright (c) 2022-present Overrun Organization
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

// This file is auto-generated. DO NOT EDIT!
package overrungl.opengl.ext.ext;

import overrungl.*;
import overrungl.opengl.*;
import java.lang.foreign.*;
import static java.lang.foreign.FunctionDescriptor.*;
import static java.lang.foreign.ValueLayout.*;
import static overrungl.opengl.GLLoader.*;

/**
 * {@code GL_EXT_texture_swizzle}
 */
public final class GLEXTTextureSwizzle {
    public static final int GL_TEXTURE_SWIZZLE_R_EXT = 0x8E42;
    public static final int GL_TEXTURE_SWIZZLE_G_EXT = 0x8E43;
    public static final int GL_TEXTURE_SWIZZLE_B_EXT = 0x8E44;
    public static final int GL_TEXTURE_SWIZZLE_A_EXT = 0x8E45;
    public static final int GL_TEXTURE_SWIZZLE_RGBA_EXT = 0x8E46;
}
