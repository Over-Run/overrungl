/*
 * MIT License
 *
 * Copyright (c) 2022-present Overrun Organization
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

// This file is auto-generated. DO NOT EDIT!
package overrungl.opengl.ext.ext;

import overrungl.*;
import overrungl.opengl.*;
import java.lang.foreign.*;
import static java.lang.foreign.FunctionDescriptor.*;
import static java.lang.foreign.ValueLayout.*;
import static overrungl.opengl.GLLoader.*;

/**
 * {@code GL_EXT_pixel_buffer_object}
 */
public final class GLEXTPixelBufferObject {
    public static final int GL_PIXEL_PACK_BUFFER_EXT = 0x88EB;
    public static final int GL_PIXEL_UNPACK_BUFFER_EXT = 0x88EC;
    public static final int GL_PIXEL_PACK_BUFFER_BINDING_EXT = 0x88ED;
    public static final int GL_PIXEL_UNPACK_BUFFER_BINDING_EXT = 0x88EF;
}
