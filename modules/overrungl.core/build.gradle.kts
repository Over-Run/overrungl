val overrunPlatformVersion: String by rootProject

dependencies {
    api("io.github.over-run:platform:$overrunPlatformVersion")
}
